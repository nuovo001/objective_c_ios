//
//  InsertViewController.h
//  RealmObjectiveC
//
//  Created by Michele Bravo on 24/02/17.
//  Copyright © 2017 Nuovo001Apple. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface InsertViewController : UIViewController

@property(nonatomic) long id;

- (IBAction)buttonSavePressed:(UIBarButtonItem *)sender;


@end
