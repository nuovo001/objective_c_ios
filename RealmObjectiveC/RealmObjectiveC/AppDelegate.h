//
//  AppDelegate.h
//  RealmObjectiveC
//
//  Created by Michele Bravo on 22/02/17.
//  Copyright © 2017 Nuovo001Apple. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;


@end

