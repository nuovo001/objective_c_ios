//
//  DetailViewController.m
//  RealmObjectiveC
//
//  Created by Michele Bravo on 01/03/17.
//  Copyright © 2017 Nuovo001Apple. All rights reserved.
//

#import "DetailViewController.h"
#import "Person.h"

@interface DetailViewController ()
@property (weak, nonatomic) IBOutlet UILabel *nameLabel;
@property (weak, nonatomic) IBOutlet UILabel *surnameLabel;
@property (weak, nonatomic) IBOutlet UILabel *birthdateLabel;
@property (weak, nonatomic) IBOutlet UILabel *emailLabel;
@property (weak, nonatomic) IBOutlet UILabel *phoneLabel;
@property (weak, nonatomic) IBOutlet UILabel *streetLabel;
@property (weak, nonatomic) IBOutlet UILabel *houseNumberLabel;
@property (weak, nonatomic) IBOutlet UILabel *cityLabel;
@property (weak, nonatomic) IBOutlet UILabel *postalCodeLabel;
@property (weak, nonatomic) IBOutlet UILabel *provinceLabel;

@end

@implementation DetailViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    NSLog(@"%@",_person );
    
    _nameLabel.text = _person.name;
    _surnameLabel.text = _person.surname;
    _birthdateLabel.text = _person.dateOfBirth;
    _emailLabel.text = _person.email;
    _streetLabel.text = _person.street;
    _houseNumberLabel.text = _person.houseNumber;
    _cityLabel.text = _person.city;
    if(_person.postalCode == NULL) {
        _postalCodeLabel.text = @"00000";
    } else {
        _postalCodeLabel.text = _person.postalCode;
    }
    _provinceLabel.text = _person.province;
    
    
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
