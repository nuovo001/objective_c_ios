//
//  InsertViewController.m
//  RealmObjectiveC
//
//  Created by Michele Bravo on 24/02/17.
//  Copyright © 2017 Nuovo001Apple. All rights reserved.
//

#import "InsertViewController.h"
#import "Person.h"


@interface InsertViewController () {
    RLMRealm *realm;
    
}
@property (weak, nonatomic) IBOutlet UITextField *nameTextField;
@property (weak, nonatomic) IBOutlet UITextField *surnameTextField;
@property (weak, nonatomic) IBOutlet UITextField *birthDateTextField;
@property (weak, nonatomic) IBOutlet UITextField *emailTextField;
@property (weak, nonatomic) IBOutlet UITextField *phoneTextField;
@property (weak, nonatomic) IBOutlet UITextField *streetTextField;
@property (weak, nonatomic) IBOutlet UITextField *houseNumberTextField;
@property (weak, nonatomic) IBOutlet UITextField *cityTextField;
@property (weak, nonatomic) IBOutlet UITextField *postalCodeTextField;
@property (weak, nonatomic) IBOutlet UITextField *provinceTextField;

@end

@implementation InsertViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    realm = [RLMRealm defaultRealm];

}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

-(BOOL)areAllPropertiesInserted {
    
    if ([self.nameTextField.text isEqualToString:@""]) {
        return NO;
    }
    if ([self.surnameTextField.text isEqualToString:@""]) {
        return NO;
    }
    if ([self.birthDateTextField.text isEqualToString:@""]) {
        return NO;
    }
    if ([self.emailTextField.text isEqualToString:@""]) {
        return NO;
    }
    if ([self.phoneTextField.text isEqualToString:@""]) {
        return NO;
    }
    if ([self.streetTextField.text isEqualToString:@""]) {
        return NO;
    }
    if ([self.houseNumberTextField.text isEqualToString:@""]) {
        return NO;
    }
    if ([self.cityTextField.text isEqualToString:@""]) {
        return NO;
    }
    if ([self.postalCodeTextField.text isEqualToString:@""]) {
        return NO;
    }
    if ([self.provinceTextField.text isEqualToString:@""]) {
        return NO;
    }
    
    return YES;
}

#pragma mark - Actions

- (IBAction)buttonSavePressed:(UIBarButtonItem *)sender {
    
    if ([self areAllPropertiesInserted]) {
        Person *person = [[Person alloc] init];
        person.id = _id;
        person.name = [NSString stringWithFormat:@"%@",self.nameTextField.text];
        person.surname = [NSString stringWithFormat:@"%@",self.surnameTextField.text];
        person.dateOfBirth = [NSString stringWithFormat:@"%@",self.birthDateTextField.text];
        person.email = [NSString stringWithFormat:@"%@",self.emailTextField.text];
        person.phoneNumber = [NSString stringWithFormat:@"%@",self.phoneTextField.text];
        person.street = [NSString stringWithFormat:@"%@",self.streetTextField.text];
        person.houseNumber = [NSString stringWithFormat:@"%@",self.houseNumberTextField.text];
        person.city = [NSString stringWithFormat:@"%@",self.cityTextField.text];
        person.postalCode= [NSString stringWithFormat:@"%@",self.postalCodeTextField.text];
        person.province = [NSString stringWithFormat:@"%@",self.provinceTextField.text];
        
        
        // Add to Realm with transaction
        [realm beginWriteTransaction];
        [realm addObject:person];
        [realm commitWriteTransaction];
        
        [self.navigationController popViewControllerAnimated:YES];

    } else {
        
        NSLog(@"non hai inserito un cazzo");
        
        NSString *message = [NSString stringWithFormat:@"Non hai inserito tutti i dati"];
        UIAlertController *alertViewController = [UIAlertController alertControllerWithTitle:@"Salvataggio non riuscito" message:message preferredStyle:UIAlertControllerStyleAlert];
        
        UIAlertAction *okAction = [UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
            //chiudi dialog
        }];
        
        [alertViewController addAction:okAction];
        [self presentViewController:alertViewController animated:true completion:nil];
        
    }
    
    }
@end
