//
//  DetailViewController.h
//  RealmObjectiveC
//
//  Created by Michele Bravo on 01/03/17.
//  Copyright © 2017 Nuovo001Apple. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "Person.h"

@interface DetailViewController : UIViewController

@property(nonatomic) Person *person;

@end
