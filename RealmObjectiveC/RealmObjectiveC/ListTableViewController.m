//
//  ListTableViewController.m
//  RealmObjectiveC
//
//  Created by Michele Bravo on 22/02/17.
//  Copyright © 2017 Nuovo001Apple. All rights reserved.
//

#import "ListTableViewController.h"

#import "Person.h"
#import "InsertViewController.h"
#import "DetailViewController.h"

#define simpleTableIdentifier @"ListCell"

@interface ListTableViewController () {
    NSArray *germanMakes;
    RLMResults<Person *> *peopleResults;
    RLMRealm *realm;

}

@end

@implementation ListTableViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    // Uncomment the following line to preserve selection between presentations.
    // self.clearsSelectionOnViewWillAppear = NO;
    
    // Uncomment the following line to display an Edit button in the navigation bar for this view controller.
    // self.navigationItem.rightBarButtonItem = self.editButtonItem;
    
    //germanMakes = [NSArray arrayWithObjects:@"Mercedes-Benz", @"BMW", @"Porsche", @"Opel", @"Volkswagen", @"Audi", nil];
    
    
    // Get the default Realm
    realm = [RLMRealm defaultRealm];
    // You only need to do this once (per thread)
    
    NSLog(@"%@",[RLMRealmConfiguration defaultConfiguration].fileURL );

}

- (void)viewDidAppear:(BOOL)animated{
    peopleResults = [[Person allObjects] sortedResultsUsingKeyPath:@"id" ascending:YES];
    [self.tableView reloadData];

}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return [peopleResults count];
}


- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:simpleTableIdentifier forIndexPath:indexPath];
    if (cell == nil) {
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:simpleTableIdentifier];
    }
    
    Person *person = [peopleResults objectAtIndex:indexPath.row];
    
    // Configure the cell...
    cell.textLabel.text = [NSString stringWithFormat:@"%@ %@", person.name, person.surname];
    cell.detailTextLabel.text = [NSString stringWithFormat:@"%li", person.id];
    
    cell.accessoryType = UITableViewCellAccessoryDisclosureIndicator;
    return cell;
}



#pragma mark - Actions

- (IBAction)addPersonClicked:(UIBarButtonItem *)sender {
    NSLog(@"addPersonClicked");
    peopleResults = [[Person allObjects] sortedResultsUsingKeyPath:@"id" ascending:YES];
    [self.tableView reloadData];
    
}





// Override to support conditional editing of the table view.
- (BOOL)tableView:(UITableView *)tableView canEditRowAtIndexPath:(NSIndexPath *)indexPath {
    // Return NO if you do not want the specified item to be editable.
    return YES;
}



// Override to support editing the table view.
- (void)tableView:(UITableView *)tableView commitEditingStyle:(UITableViewCellEditingStyle)editingStyle forRowAtIndexPath:(NSIndexPath *)indexPath {
    if (editingStyle == UITableViewCellEditingStyleDelete) {
        // Delete the row from the data source
        Person *person = [peopleResults objectAtIndex:indexPath.row];
        
        [realm beginWriteTransaction];
        [realm deleteObject:person];
        [realm commitWriteTransaction];
        [tableView deleteRowsAtIndexPaths:@[indexPath] withRowAnimation:UITableViewRowAnimationFade];
       
        //peopleResults
        
    } else if (editingStyle == UITableViewCellEditingStyleInsert) {
        // Create a new instance of the appropriate class, insert it into the array, and add a new row to the table view
    }   
}


/*
// Override to support rearranging the table view.
- (void)tableView:(UITableView *)tableView moveRowAtIndexPath:(NSIndexPath *)fromIndexPath toIndexPath:(NSIndexPath *)toIndexPath {
}
*/

/*
// Override to support conditional rearranging of the table view.
- (BOOL)tableView:(UITableView *)tableView canMoveRowAtIndexPath:(NSIndexPath *)indexPath {
    // Return NO if you do not want the item to be re-orderable.
    return YES;
}
*/


#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
 -(void)prepareForSegue:(UIStoryboardSegue *)segue sender:(UITableViewCell *)cell {
 
     if ([[segue identifier] isEqualToString:@"InsertDataSegue"]) {
         NSInteger lastObjectId;
         if(peopleResults.count > 0)
             lastObjectId = [[peopleResults sortedResultsUsingKeyPath:@"id" ascending:NO] objectAtIndex:0].id + 1;
         else
             lastObjectId = 1;
         
         NSLog(@"%li", (long)lastObjectId);
         InsertViewController *insertViewController = [segue destinationViewController];
         insertViewController.id = lastObjectId;
     }
     
     
     
     if ([[segue identifier] isEqualToString:@"DetailDataSegue"]) {
         DetailViewController *detailView = [segue destinationViewController];
         
         long arrayIndex = [self.tableView indexPathForSelectedRow].row;
         
         detailView.person = [peopleResults objectAtIndex:arrayIndex];
         
     }
     
     
 }

@end
