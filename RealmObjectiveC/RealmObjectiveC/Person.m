//
//  Person.m
//  RealmObjectiveC
//
//  Created by Michele Bravo on 23/02/17.
//  Copyright © 2017 Nuovo001Apple. All rights reserved.
//

#import "Person.h"
#import <Realm/Realm.h>


@implementation Person
+ (NSString *)primaryKey {
    return @"id";
}

@end
