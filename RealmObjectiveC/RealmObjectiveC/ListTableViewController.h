//
//  ListTableViewController.h
//  RealmObjectiveC
//
//  Created by Michele Bravo on 22/02/17.
//  Copyright © 2017 Nuovo001Apple. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ListTableViewController : UITableViewController
- (IBAction)addPersonClicked:(UIBarButtonItem *)sender;

@end
