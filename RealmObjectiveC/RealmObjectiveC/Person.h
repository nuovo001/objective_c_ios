//
//  Person.h
//  RealmObjectiveC
//
//  Created by Michele Bravo on 23/02/17.
//  Copyright © 2017 Nuovo001Apple. All rights reserved.
//

#import <Realm/Realm.h>


@interface Person : RLMObject
@property NSInteger id;
@property NSString *name;
@property NSString *surname;
@property NSString *dateOfBirth;
@property NSString *email;
@property NSString *phoneNumber;
@property NSString *street;
@property NSString *houseNumber;
@property NSString *city;
@property NSString *postalCode;
@property NSString *province;
@end
RLM_ARRAY_TYPE(Person)

