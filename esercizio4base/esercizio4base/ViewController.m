//
//  ViewController.m
//  esercizio4base
//
//  Created by Michele Bravo on 24/01/17.
//  Copyright © 2017 Nuovo001Apple. All rights reserved.
//

#import "ViewController.h"
#define Defaults [NSUserDefaults standardUserDefaults]
#define Elements @"array_elements"

@interface ViewController ()

@end

@implementation ViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view, typically from a nib.
    
    //creo array
    NSArray *array = [self generateRandomArrayElements];
    NSLog(@"l'array è: %@", array);
    
    //sommo array
    NSNumber * sum = [array valueForKeyPath:@"@sum.self"];
    [self.sumLabel setText:[NSString stringWithFormat:@"La somma dell'array è: %@", sum]];
    NSLog(@"La somma dell'array è: %@", sum);
    
    //ordine crescente
    NSArray *ascendingArray = [self orderArrayAscending:array];
    NSLog(@"Ordine crescente: %@", ascendingArray);
    
    //ordine decrescente
    NSArray *descendingArray = [self orderArrayDescending:array];
    NSLog(@"Ordine decrescente: %@", descendingArray);
    
}


- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(int)getRandomNumberBetween:(int)from to:(int)to {
    return (int)from + arc4random() % (to-from+1);
}

-(NSArray *)generateRandomArrayElements {
    NSMutableArray *array = @[].mutableCopy;
    for (int i=0; i<=100; i++) {
        [array addObject:@([self getRandomNumberBetween:0 to:100])];
    }
    
    return array;
}

-(NSArray *)orderArrayAscending:(NSArray *)array {
    NSArray *ascendingArray = [array sortedArrayUsingComparator:^NSComparisonResult(NSNumber *obj1, NSNumber *obj2) {
        int value1 = obj1.intValue;
        int value2 = obj2.intValue;
        
        if (value1 == value2) {
            return NSOrderedSame;
        }
        
        if (value1 < value2) {
            return NSOrderedAscending;
        }
        
        return NSOrderedDescending;
    }];
    
    return ascendingArray;
    
}

-(NSArray *)orderArrayDescending:(NSArray *)array {
    
    
    NSArray *descendingArray = [array sortedArrayUsingComparator:^NSComparisonResult(NSNumber *obj1, NSNumber *obj2) {
        int value1 = obj1.intValue;
        int value2 = obj2.intValue;
        
        if (value1 == value2) {
            return NSOrderedSame;
        }
        
        if (value1 < value2) {
            return NSOrderedDescending;
        }
        
        return NSOrderedAscending;
    }];
    
    return descendingArray;

    
}


@end
