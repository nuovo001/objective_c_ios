//
//  ViewController.m
//  esercizio5base
//
//  Created by Michele Bravo on 24/01/17.
//  Copyright © 2017 Nuovo001Apple. All rights reserved.
//

#import "ViewController.h"

@interface ViewController ()

@end

@implementation ViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view, typically from a nib.
    
    NSArray *names = [NSArray arrayWithObjects: @"Freddie", @"Krista", @"Merle", @"Lorraine", @"Lorenzo", @"Sandra", @"Nichole", @"Floyd", @"Andrea", @"Marcus", @"Jacob", @"Peter", @"Glenn", @"Francis", @"Yvette", @"Evan", @"Shelley", @"Gladys", @"Stella", @"Mindy", @"Beth", @"Everett", @"Audrey", @"Christie", @"Timothy", nil];
    NSArray *surnames = [NSArray arrayWithObjects: @"Campbell", @"Kim", @"Hubbard", @"Weber", @"Greene", @"Brock", @"Hines", @"Clarke", @"Ramsey", @"Reid", @"Garza", @"Herrera", @"Horton", @"Mack", @"Farmer", @"Hughes", @"Lane", @"Carroll", @"Thornton", @"Wilkerson", @"Diaz", @"Mckinney", @"Cooper", @"Sutton", @"Walters", nil];
    
    [self logArray:surnames andNames:names];
    
    
    NSMutableArray *permutation = [NSMutableArray arrayWithCapacity:names.count];
    for (NSUInteger i = 0 ; i != surnames.count ; i++) {
        [permutation addObject:[NSNumber numberWithInteger:i]];
    }
    [permutation sortWithOptions:0 usingComparator:^NSComparisonResult(id obj1, id obj2) {
        // Modify this to use [first objectAtIndex:[obj1 intValue]].name property
        NSString *lhs = [surnames objectAtIndex:[obj1 intValue]];
        // Same goes for the next line: use the name
        NSString *rhs = [surnames objectAtIndex:[obj2 intValue]];
        return [lhs compare:rhs];
    }];
    NSMutableArray *sortedSurnames = [NSMutableArray arrayWithCapacity:surnames.count];
    NSMutableArray *sortedNames = [NSMutableArray arrayWithCapacity:surnames.count];
    [permutation enumerateObjectsUsingBlock:^(id obj, NSUInteger idx, BOOL *stop) {
        NSUInteger pos = [obj intValue];
        [sortedSurnames addObject:[surnames objectAtIndex:pos]];
        [sortedNames addObject:[names objectAtIndex:pos]];
    }];
    
    [self logArray:sortedSurnames andNames:sortedNames];
}


- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(void)logArray:(NSArray *)surnames andNames:(NSArray *)names {
    
    NSMutableString *printString = [[NSMutableString alloc] init];
    
    [printString appendString:@"\n"];
    
    for (int i=0; i<surnames.count; i++) {
        
        [printString appendString:[NSString stringWithFormat:@"%@ %@ \n", [surnames objectAtIndex:i], [names objectAtIndex:i]]];

    }
    
    NSLog(@"%@", printString);
    
}


@end
