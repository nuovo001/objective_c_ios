//
//  ViewController.m
//  esercizio7base
//
//  Created by Michele Bravo on 24/01/17.
//  Copyright © 2017 Nuovo001Apple. All rights reserved.
//

#import "ViewController.h"

@interface ViewController () {
    NSTimer *timer;
    int _sec, _min, _hours;
}

@end

@implementation ViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view, typically from a nib.
    
    _sec = 0;
    _min = 0;
    _hours = 0;
    [self.timerLabel setText:[NSString stringWithFormat:@"0%i:0%i:0%i",_hours, _min, _sec]];
    
    if(timer == nil)  {
        timer = [NSTimer scheduledTimerWithTimeInterval:1.0 target:self
                                                    selector:@selector(timerTick) userInfo:nil repeats:true];
    }
}


- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(void)timerTick {
    NSMutableString *timerString = [[NSMutableString alloc] init];
    _sec++;
    
    if (_sec == 60) {
        _sec = 0;
        _min++;
        if (_min == 60) {
            _min = 0;
            _hours++;
        }
    }
    
    if (_hours <10) {
        [timerString appendString:[NSString stringWithFormat:@"0%i:",_hours]];
    } else {
        [timerString appendString:[NSString stringWithFormat:@"%i:",_hours]];
    }
    
    if (_min <10) {
        [timerString appendString:[NSString stringWithFormat:@"0%i:",_min]];
    } else {
        [timerString appendString:[NSString stringWithFormat:@"%i:",_min]];
    }
    
    if (_sec <10) {
        [timerString appendString:[NSString stringWithFormat:@"0%i",_sec]];
    } else {
        [timerString appendString:[NSString stringWithFormat:@"%i",_sec]];
    }
    
    
    [self.timerLabel setText:[NSString stringWithFormat:@"%@",timerString]];
}


@end
