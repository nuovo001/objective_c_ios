//
//  ViewController.h
//  esercizio2base
//
//  Created by Michele Bravo on 23/01/17.
//  Copyright © 2017 Nuovo001Apple. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ViewController : UIViewController

@property(nonatomic, weak) IBOutlet UILabel *evenOddLabel;

@property(nonatomic, weak) IBOutlet UITextField *insertNumberTextView;

-(IBAction)printEvenOrOddNUmberDidOnEndOnExit:(id)sender;


@end

