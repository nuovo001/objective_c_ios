//
//  ViewController.m
//  esercizio2base
//
//  Created by Michele Bravo on 23/01/17.
//  Copyright © 2017 Nuovo001Apple. All rights reserved.
//

#import "ViewController.h"

@interface ViewController ()

@end

@implementation ViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view, typically from a nib.
}


- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - Actions
-(IBAction)printEvenOrOddNUmberDidOnEndOnExit:(UITextField *)sender {
    
    if ([sender.text intValue] % 2) {
        
        [self.evenOddLabel setText:@"DISPARI"];
        NSLog(@"DISPARI");
        
    } else {
        
        [self.evenOddLabel setText:@"PARI"];
        NSLog(@"PARI");
        
    }
    
}


@end
