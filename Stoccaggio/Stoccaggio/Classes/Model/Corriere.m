//
//  Corriere.m
//  Stoccaggio
//
//  Created by Michele Bravo on 09/02/17.
//  Copyright © 2017 Nuovo001Apple. All rights reserved.
//

#import "Corriere.h"

@implementation Corriere

-(id)initWithCodice:(NSString *)codice
               nome:(NSString *)nome
            cognome:(NSString *)cognome
          andVolume:(NSInteger)volume{
    
    self = [super init];
    
    if(self) {
        _codice = codice;
        _nome = nome;
        _cognome = cognome;
        _volume = volume;
    }
    
    return self;
    
}

@end
