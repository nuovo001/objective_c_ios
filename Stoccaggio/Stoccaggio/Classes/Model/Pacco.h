//
//  Pacco.h
//  Stoccaggio
//
//  Created by Michele Bravo on 08/02/17.
//  Copyright © 2017 Nuovo001Apple. All rights reserved.
//

#import <Foundation/Foundation.h>

typedef NS_ENUM(NSInteger, Materiale) {
    MaterialeFerro = 0,
    MaterialePlastica = 1,
    MaterialeCarta = 2
};

@interface Pacco : NSObject

-(id)initWithCodice:(NSString *)codice
           mittente:(NSString *)mittente
       destinatario:(NSString *)destinatario
          lunghezza:(NSInteger)lunghezza
            altezza:(NSInteger)altezza
          larghezza:(NSInteger)larghezza
       andMateriale:(Materiale)materiale;

/// Il codice identificativo del pacco
@property (nonatomic, strong, readonly) NSString *codice;

/// il mittente che ha spedito il pacco
@property (nonatomic, strong, readonly) NSString *mittente;

/// il destinatario che riceve il pacco
@property (nonatomic, strong, readonly) NSString *destinatario;

/// le dimensioni del pacco
@property (nonatomic, readonly) NSInteger lunghezza;
@property (nonatomic, readonly) NSInteger altezza;
@property (nonatomic, readonly) NSInteger larghezza;

/// il volume del pacco
@property (nonatomic, readonly) NSInteger volume;

/// il peso del pacco
@property (nonatomic, readonly) NSInteger peso;

/// Tipologia Materiale contenuto nel pacco
@property (nonatomic) Materiale materiale;



@end
