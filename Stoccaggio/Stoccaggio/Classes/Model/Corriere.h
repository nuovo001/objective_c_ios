//
//  Corriere.h
//  Stoccaggio
//
//  Created by Michele Bravo on 09/02/17.
//  Copyright © 2017 Nuovo001Apple. All rights reserved.
//

#import <Foundation/Foundation.h>



@interface Corriere : NSObject

-(id)initWithCodice:(NSString *)codice
               nome:(NSString *)nome
            cognome:(NSString *)cognome
          andVolume:(NSInteger)volume;


/// Il codice identificativo del pacco
@property (nonatomic, strong, readonly) NSString *codice;

/// il mittente che ha spedito il pacco
@property (nonatomic, strong, readonly) NSString *nome;

/// il mittente che ha spedito il pacco
@property (nonatomic, strong, readonly) NSString *cognome;

@property (nonatomic, readonly) NSInteger volume;

@end
