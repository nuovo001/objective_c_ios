//
//  Pacco.m
//  Stoccaggio
//
//  Created by Michele Bravo on 08/02/17.
//  Copyright © 2017 Nuovo001Apple. All rights reserved.
//

#import "Pacco.h"

#import "Utils.h"

@implementation Pacco
-(id)initWithCodice:(NSString *)codice
           mittente:(NSString *)mittente
       destinatario:(NSString *)destinatario
          lunghezza:(NSInteger)lunghezza
            altezza:(NSInteger)altezza
          larghezza:(NSInteger)larghezza
       andMateriale:(Materiale)materiale {
    
    /// chiamo il costruttore del mio oggetto da cui eredito (in questo caso NSObject)
    self = [super init];
    
    /// controllo se il costruttore padre ha allocato il mio oggetto
    if (self) {
        // fse tutto è ok procedo con l'iniziallizazione
        _codice = codice;
        _mittente = mittente;
        _destinatario = destinatario;
        _lunghezza = lunghezza;
        _altezza = altezza;
        _larghezza = larghezza;
        _materiale = materiale;
    }
    
    return self;
}

#pragma mark - Getters

-(NSInteger)volume {
    return _lunghezza*_altezza*_larghezza;
}

-(NSInteger)peso {
    switch (_materiale) {
        case 0:
            return self.volume*kPesoSpecificoFerro/1000;
            break;
            
        case 1:
            return self.volume*kPesoSpecificoPlastica/1000;
            break;
            
        case 2:
            return self.volume*kPesoSpecificoCarta/1000;
            break;
    }
}

#pragma mark - Overrides

-(NSString *)description {
    NSString *newDescription = [NSString stringWithFormat:@"%@,\nCodice: %@\nMittente: %@\nDestinatario: %@\nLunghezza: %licm\nAltezza: %licm\nLarghezza %licm\nMateriale %li\nVolume: %licm3\nPeso: %likg", [super description], self.codice, self.mittente, self.destinatario, self.lunghezza, self.altezza, self.larghezza, self.materiale, self.volume, self.peso];
    
    return newDescription;
}

@end
