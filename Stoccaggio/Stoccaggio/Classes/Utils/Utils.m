//
//  Utils.m
//  Stoccaggio
//
//  Created by Michele Bravo on 08/02/17.
//  Copyright © 2017 Nuovo001Apple. All rights reserved.
//

#import "Utils.h"

///peso ferro in grammi su cm3
const NSInteger kPesoSpecificoFerro = 8;

///peso plastica in grammi su cm3
const NSInteger kPesoSpecificoPlastica = 2;

///peso carta in grammi su cm3
const NSInteger kPesoSpecificoCarta = 1;

@implementation Utils

@end
