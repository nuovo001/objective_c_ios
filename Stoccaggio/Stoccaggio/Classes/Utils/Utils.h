//
//  Utils.h
//  Stoccaggio
//
//  Created by Michele Bravo on 08/02/17.
//  Copyright © 2017 Nuovo001Apple. All rights reserved.
//

#import <Foundation/Foundation.h>

FOUNDATION_EXPORT const NSInteger kPesoSpecificoFerro;

FOUNDATION_EXPORT const NSInteger kPesoSpecificoPlastica;

FOUNDATION_EXPORT const NSInteger kPesoSpecificoCarta;

@interface Utils : NSObject

@end


