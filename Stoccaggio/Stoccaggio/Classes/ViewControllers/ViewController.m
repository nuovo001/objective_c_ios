//
//  ViewController.m
//  Stoccaggio
//
//  Created by Michele Bravo on 08/02/17.
//  Copyright © 2017 Nuovo001Apple. All rights reserved.
//

#import "ViewController.h"

#import "Pacco.h"

@interface ViewController ()

@end

@implementation ViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view, typically from a nib.
    
    Pacco *array[100];
    
    for (int i = 0; i < 100; i++) {
        
        array[i] = [[Pacco alloc] initWithCodice:[NSString stringWithFormat:@"%i",i]
                                        mittente:@"giuseppe"
                                    destinatario:@"francesco"
                                       lunghezza:arc4random_uniform(100)
                                         altezza:arc4random_uniform(100)
                                       larghezza:arc4random_uniform(100)
                                    andMateriale:arc4random_uniform(2)];
        
        NSLog(@"Pacco n.%i: %@", i, array[i]);
    }

    
}



- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


@end
