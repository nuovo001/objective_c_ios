//
//  ViewController.m
//  esercizio3base
//
//  Created by Michele Bravo on 23/01/17.
//  Copyright © 2017 Nuovo001Apple. All rights reserved.
//

#import "ViewController.h"

@interface ViewController ()

@end

@implementation ViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view, typically from a nib.
}


- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(int)getRandomNumberBetween:(int)from to:(int)to {
    return (int)from + arc4random() % (to-from+1);
}

-(void)getCorrectPopup {
    
    UIAlertController *alertViewController = [UIAlertController alertControllerWithTitle:@"Corretto" message:@"Hai vinto!" preferredStyle: UIAlertControllerStyleAlert];
    
    UIAlertAction *okAction = [UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
        
        NSLog(@"OK Correct Action premuta");
    }];
    
    [alertViewController addAction:okAction];
    
    
    [self presentViewController:alertViewController animated:true completion:nil];
    
}

-(void)getUncorrectPopup {
    
    UIAlertController *alertViewController = [UIAlertController alertControllerWithTitle:@"Sbagliato" message:@"Hai perso!" preferredStyle: UIAlertControllerStyleAlert];
    
    UIAlertAction *okAction = [UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
        
        NSLog(@"OK Uncorrect Action premuta");
    }];
    
    [alertViewController addAction:okAction];
    
    
    [self presentViewController:alertViewController animated:true completion:nil];
    
}


#pragma mark - Actions

-(IBAction)buttonPressedStartGame:(id)sender {
    
    int _randomNumber = [self getRandomNumberBetween:0 to:100];
    
    NSString *title = [NSString stringWithFormat:@"%i", _randomNumber];
    
    NSString *message = [NSString stringWithFormat:@"Il numero estratto è pari?"];
    
    UIAlertController *alertViewController = [UIAlertController alertControllerWithTitle:title message:message preferredStyle: UIAlertControllerStyleAlert];
    
    UIAlertAction *yesAction = [UIAlertAction actionWithTitle:@"SI" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
        
        NSLog(@"Yes Action premuta");
        
        if (_randomNumber % 2) {
            [self getUncorrectPopup];
        } else {
            [self getCorrectPopup];

        }
    }];
    
    [alertViewController addAction:yesAction];
    
    UIAlertAction *noAction = [UIAlertAction actionWithTitle:@"No" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
        
        NSLog(@"No Action premuta");
        
        if (_randomNumber % 2) {
            [self getCorrectPopup];
        } else {
            [self getUncorrectPopup];
    
        }
    }];
    
    [alertViewController addAction:noAction];
    
    [self presentViewController:alertViewController animated:true completion:nil];
    
}


@end
