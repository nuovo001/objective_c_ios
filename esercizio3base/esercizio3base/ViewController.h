//
//  ViewController.h
//  esercizio3base
//
//  Created by Michele Bravo on 23/01/17.
//  Copyright © 2017 Nuovo001Apple. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ViewController : UIViewController


-(IBAction)buttonPressedStartGame:(id)sender;

@end

