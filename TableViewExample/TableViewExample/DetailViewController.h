//
//  DetailViewController.h
//  TableViewExample
//
//  Created by Michele Bravo on 28/02/17.
//  Copyright © 2017 Nuovo001Apple. All rights reserved.
//

#import <UIKit/UIKit.h>

@protocol detailViewControllerDelegate <NSObject>

-(void) didChangeValueForObject:(NSMutableDictionary *)dic;

@end

@interface DetailViewController : UIViewController 

@property NSMutableDictionary *detailDictionary;
@property id <detailViewControllerDelegate> delegate;

@end



