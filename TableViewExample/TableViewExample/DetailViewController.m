//
//  DetailViewController.m
//  TableViewExample
//
//  Created by Michele Bravo on 28/02/17.
//  Copyright © 2017 Nuovo001Apple. All rights reserved.
//

#import "DetailViewController.h"

@interface DetailViewController ()<UITextFieldDelegate>

@property (weak, nonatomic) IBOutlet UITextField *idTextField;

@property (weak, nonatomic) IBOutlet UITextField *titleTextField;

@property (weak, nonatomic) IBOutlet UITextField *userTextField;

@end


@implementation DetailViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    self.idTextField.delegate = self;
    self.titleTextField.delegate = self;
    self.userTextField.delegate = self;
    
    self.idTextField.text = [NSString stringWithFormat:@"%@",[_detailDictionary objectForKey:@"id"]];
     self.titleTextField.text = [NSString stringWithFormat:@"%@",[_detailDictionary objectForKey:@"title"]];
     self.userTextField.text = [NSString stringWithFormat:@"%@",[_detailDictionary objectForKey:@"userId"]];
    
}

-(BOOL)textFieldShouldReturn:(UITextField *)textField {
    
    self.detailDictionary = [NSMutableDictionary dictionaryWithDictionary:@{
                                          @"id":self.idTextField.text,
                                          @"title":self.titleTextField.text,
                                          @"userId":self.userTextField.text
                                          }];
    NSLog(@"Value %@", self.detailDictionary);
    [textField resignFirstResponder];
    
    return true;
}


-(void)touchesBegan:(NSSet<UITouch *> *)touches withEvent:(UIEvent *)event {
    [self.view endEditing:YES];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(void)viewWillDisappear:(BOOL)animated{
    if ([self.delegate respondsToSelector:@selector(didChangeValueForObject:)]) {
        [self.delegate didChangeValueForObject:self.detailDictionary];
        
    }
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/


@end
