//
//  ViewController.m
//  esercizio6base
//
//  Created by Michele Bravo on 24/01/17.
//  Copyright © 2017 Nuovo001Apple. All rights reserved.
//

#import "ViewController.h"

@interface ViewController ()

@end

@implementation ViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view, typically from a nib.
}


- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


#pragma mark - Actions


-(IBAction)nameInsertTextFieldEditingChanged:(UITextField *)sender {
    
    name = sender.text;
    
}
-(IBAction)surnameInsertTextFieldEditingChanged:(UITextField *)sender {
    
    surname = sender.text;
    
}
-(IBAction)ageInsertTextFieldEditingChanged:(UITextField *)sender {
    
    age = [sender.text integerValue];
}

-(IBAction)buttonPressed:(id)sender {
    NSLog(@"NOME: %@ \n COGNOME: %@ \n ETA': %li",name,surname,(long)age);

}

@end

