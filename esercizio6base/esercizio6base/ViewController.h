//
//  ViewController.h
//  esercizio6base
//
//  Created by Michele Bravo on 24/01/17.
//  Copyright © 2017 Nuovo001Apple. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ViewController : UIViewController {
    NSString *name;
    NSString *surname;
    NSInteger age;
}

@property(nonatomic, weak) IBOutlet UITextField *nameInsertTextField;
@property(nonatomic, weak) IBOutlet UITextField *surnameInsertTextField;
@property(nonatomic, weak) IBOutlet UITextField *ageInsertTextField;

-(IBAction)nameInsertTextFieldEditingChanged:(id)sender;
-(IBAction)surnameInsertTextFieldEditingChanged:(id)sender;
-(IBAction)ageInsertTextFieldEditingChanged:(id)sender;
-(IBAction)buttonPressed:(id)sender;


@end

