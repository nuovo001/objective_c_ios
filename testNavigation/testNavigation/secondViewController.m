//
//  secondViewController.m
//  testNavigation
//
//  Created by Michele Bravo on 07/02/17.
//  Copyright © 2017 Nuovo001Apple. All rights reserved.
//

#import "secondViewController.h"

@interface secondViewController ()

@end

@implementation secondViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    [self.colorView setBackgroundColor:_color];
    CGFloat *width = self.colorView.frame.size.width;
    [self.colorView.layer setCornerRadius:width/2];
}


- (IBAction)backButoonClicked:(UIButton *)sender {
    [self dismissViewControllerAnimated:YES
                             completion:^{
                                 NSLog(@"Fatto");
                             }
     ];
}


- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
