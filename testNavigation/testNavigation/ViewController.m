//
//  ViewController.m
//  testNavigation
//
//  Created by Michele Bravo on 07/02/17.
//  Copyright © 2017 Nuovo001Apple. All rights reserved.
//

#import "ViewController.h"
#import "secondViewController.h"

@interface ViewController ()

@end

@implementation ViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view, typically from a nib.
    
    NSTimer *timer = [[NSTimer alloc] initWithFireDate:[NSDate date] interval:5000 repeats:NO block:^(NSTimer * _Nonnull timer) {
        [self doTheSegueForMe];
    }];
    [timer fire];
}

-(void) doTheSegueForMe {
    
    [self performSegueWithIdentifier:@"controllerSegue" sender:nil];
}

- (IBAction)returnIntoFirstController:(UIStoryboardSegue *)segue {
    
}

-(void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    
    UIButton *button;
    
    UIColor *color = [UIColor yellowColor];
    
    if ([segue.identifier isEqual:@"buttonSegue"]) {
        if ([sender isKindOfClass:[UIButton class]]) {
            button = (UIButton*) sender;
            NSLog(@"Ho premuto il pulsante! %@", button);
            color = [UIColor redColor];
        }
    } else if ([segue.identifier isEqualToString:@"controllerSegue"]) {
        NSLog(@"Ho fatto il segue del controller");
        color = [UIColor blueColor];
    }
    
    secondViewController *secondVC =segue.destinationViewController;
    [secondVC setColor:color];
    
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


@end
