//
//  secondViewController.h
//  testNavigation
//
//  Created by Michele Bravo on 07/02/17.
//  Copyright © 2017 Nuovo001Apple. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface secondViewController : UIViewController

@property (weak, nonatomic) IBOutlet UIView *colorView;

@property UIColor *color;

@end
