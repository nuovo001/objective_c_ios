//
//  ViewController.h
//  SingleViewApplication
//
//  Created by Michele Bravo on 11/01/17.
//  Copyright © 2017 Nuovo001Apple. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ViewController : UIViewController

@property (nonatomic, weak) IBOutlet UILabel *helloWorldLabel;

@property (nonatomic, weak) IBOutlet UITextField *userNameTextField;


-(IBAction)userNameTextFieldDidEndOnExit:(id)sender;

-(IBAction)userNameTextFieldEditingDidEnd:(id)sender;

-(IBAction)buttonPressed:(id)sender;


@end

