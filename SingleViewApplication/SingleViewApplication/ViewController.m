//
//  ViewController.m
//  SingleViewApplication
//
//  Created by Michele Bravo on 11/01/17.
//  Copyright © 2017 Nuovo001Apple. All rights reserved.
//

#import "ViewController.h"

@interface ViewController () {
    int _tapCount;
}

@end

@implementation ViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    //NSString *stringa =@"ciao mondo";
    //int intero = 2;
    
    //NSLog(@"Hello world -> %@, %i, \n %@", @"daniele", intero, stringa);
    NSLog(@"2");
    
    [self.userNameTextField setText:@"Dario"];
    
    _tapCount = 0;
    // Do any additional setup after loading the view, typically from a nib.
}

- (void)viewDidAppear:(BOOL)animated{
    NSLog(@"4");
}

-(void)viewWillAppear:(BOOL)animated{
    NSLog(@"3");
}

-(void)awakeFromNib{
    [super awakeFromNib];
    NSLog(@"1");
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - Actions

-(IBAction)userNameTextFieldDidEndOnExit:(id)sender{
    
    NSLog(@"userNameTextFieldDidEndOnExit");
    
}

-(IBAction)userNameTextFieldEditingDidEnd:(UITextField *)sender{
    NSLog(@"userNameTextFieldEditingDidEnd");
    
    NSLog(@"testo scritto dell'utente : %@ ", sender.text);
    
}

-(IBAction)buttonPressed:(id)sender{
    NSLog(@"button pressed correctly");
    _tapCount++;
    NSLog(@"tapCount -> %i", _tapCount);
    
    [self.helloWorldLabel setText:
     [NSString stringWithFormat:@"%i", _tapCount]];
}





@end
