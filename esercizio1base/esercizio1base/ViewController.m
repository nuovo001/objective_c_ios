//
//  ViewController.m
//  esercizio1base
//
//  Created by Michele Bravo on 23/01/17.
//  Copyright © 2017 Nuovo001Apple. All rights reserved.
//

#import "ViewController.h"

@interface ViewController () {
}

@end

@implementation ViewController


- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view, typically from a nib.
    
}


- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


#pragma mark - Actions


-(IBAction)textFieldEditingDidEndOnExit:(UITextField *)sender {
    
        [self.printLabel setText: [NSString stringWithFormat:@"Nome: %@", sender.text]];
        NSLog(@"Nome : %@ ", sender.text);
    
        
    
}

@end
