//
//  ViewController.h
//  esercizio1base
//
//  Created by Michele Bravo on 23/01/17.
//  Copyright © 2017 Nuovo001Apple. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ViewController : UIViewController

@property(nonatomic, weak) IBOutlet UILabel *printLabel;
@property(nonatomic, weak) IBOutlet UITextField *textField;

-(IBAction)textFieldEditingDidEndOnExit:(id)sender;


@end

